
#
# Makefile variables
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
#

tmp := tmp
key := $(tmp)/id_rsa
version := PA-VM-7.1.0
img := $(tmp)/$(version).ova
ssh := 2222
https := 8443
user := admin
pass := admin
box := $(tmp)/$(version).box
name := msia/$(version)


.PHONY: default
default:
	@echo 'default task not implemented'

$(tmp):
	mkdir $(tmp)

$(tmp)/id_rsa: | $(tmp)
	@echo '* Generating authentication keys...' ; \
	ssh-keygen -f $(key) -t rsa -N ''

$(key).pub:| $(key)

# Vagrant Tasks
$(box): | vbox-start
	@echo '* Packaging vagrant basebox...' ; \
	vagrant package --base $(version) --output $(box)

.PHONY: vagrant-package
vagrant-package: $(box)
	vagrant box add --name $(name) $(box)

.PHONY: vagrant-clean
vagrant-clean:
	@if vagrant box list | grep $(name) > /dev/null 2>&1 ; then \
		echo '* Removing vagrant box...' ; \
		vagrant box remove $(name) ; \
	fi
	if [[ -f $box ]] ; then rm $(box) ; fi


# VirtualBox Tasks
.PHONY: vbox-import
vbox-import:
	@if ! vboxmanage list vms | grep $(version) > /dev/null 2>&1 ; then \
		echo '* Importing machine image...' ; \
		vboxmanage import $(img) ; \
	fi

.PHONY: vbox-start
vbox-start: vbox-import $(key).pub
	@if ! vboxmanage list runningvms | grep $(version) > /dev/null 2>&1 ; then \
		echo '* Configuring machine settings...' ; \
		if ! vboxmanage list hostonlyifs | grep 'vboxnet0' ; then \
			vboxmanage hostonlyif create ; \
		fi ; \
		if ! vboxmanage showvminfo $(version) | grep 'NIC 1.*Attachment: NAT' ; then \
			vboxmanage modifyvm $(version) --nic1 nat ; \
		fi ; \
		if ! vboxmanage showvminfo $(version) | grep 'NIC.*Rule.*ssh' ; then \
			vboxmanage modifyvm $(version) --natpf1 "ssh,tcp,,$(ssh),,22" ; \
		fi ; \
		if ! vboxmanage showvminfo $(version) | grep 'NIC.*Rule.*https' ; then \
			vboxmanage modifyvm $(version) --natpf1 "https,tcp,,$(https),,443" ; \
		fi ; \
		if ! vboxmanage showvminfo $(version) | grep "NIC 2.*Attachment: Host-only Interface \'vboxnet0\'" ; then \
			vboxmanage modifyvm $(version) --nic2 hostonly --hostonlyadapter2 vboxnet0 ; \
		fi ; \
		echo '* Starting machine...' ; \
		vboxmanage startvm $(version) ; \
	fi
	@while ! curl -k --connect-timeout 10 https://localhost:$(https) > /dev/null 2>&1 ; do \
		echo -n "." ; sleep 5 ; \
	done
	bin/vbox-config $(version) $(user) $(pass) $(key).pub

.PHONY: vbox-poweroff
vbox-poweroff:
	@if vboxmanage list runningvms | grep $(version) > /dev/null 2>&1 ; then \
		echo '* Forcing machine off...' ; \
		vboxmanage controlvm $(version) poweroff; \
	fi

.PHONY: vbox-clean
vbox-clean: vbox-poweroff
	@if vboxmanage list vms | grep $(version) > /dev/null 2>&1 ; then \
		echo '* Removing machine...' ; \
		vboxmanage unregistervm $(version) --delete ; \
	fi
