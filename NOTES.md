
# PaloAlto VM Docs
https://www.paloaltonetworks.com/documentation/71/virtualization
https://www.paloaltonetworks.com/documentation/71/virtualization/virtualization

## VM MAC Address?
https://www.paloaltonetworks.com/documentation/70/pan-os/newfeaturesguide/virtualization-features/support-for-hypervisor-assigned-mac-addresses

# PANOS CLI
https://www.paloaltonetworks.com/documentation/71/pan-os/cli-gsg/cli-cheat-sheets/cli-cheat-sheet-networking

# PANOS API
https://www.paloaltonetworks.com/documentation/71/pan-os/xml-api/get-started-with-the-pan-os-xml-api/explore-the-api

## POST
https://www.paloaltonetworks.com/documentation/71/pan-os/xml-api/about-the-pan-os-xml-api/structure-of-a-pan-os-xml-api-request

## API Error Codes
https://www.paloaltonetworks.com/documentation/71/pan-os/xml-api/pan-os-xml-api-error-codes

# PANOS Load Partial Config
https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Load-Partial-Configurations/ta-p/62549

# PANOS Ansible
https://live.paloaltonetworks.com/t5/Ansible/ct-p/Ansible
http://panwansible.readthedocs.io/en/latest/
https://github.com/PaloAltoNetworks/ansible-pan/

# Create a vagrant basebox
https://www.vagrantup.com/docs/virtualbox/boxes.html

# BATS
https://github.com/sstephenson/bats

# Methodology

## Platform

Virtualization
- Commercial Cloud
	- Amazon AWS
	- Google Cloud
	- Rackspace (OpenStack)
- Enterprise/Personal Cloud
	- VMware ESXi/vCenter/etc.
	- OpenStack
- Single System Hypervisor
	- Cross Platform
		- VMware Workstation
		- Virtualbox
	- Linux Only
		- Xen
		- KVM

Workstation
- Linux
- Windows
	- w/ subsystem for Linux
	- with Linux VM for tools

Tools
- Network Simulation
	- Vagrant (https://www.vagrantup.com)
	- Terraform (https://www.terraform.io)
	- Cloudformation (AWS, OpenStack)
- Testing
	- Bash scripts, Shunit2, BATS
	- Python, unittest
	- Ruby, rspec, serverspec



## Procedures

Simulation
- design simulated network architecture
	- firewall, 2 interfaces, test from hostname
	- firewall, server DMZ
	- firewall, server DMZ, internal PC's
- get firewall working on virt platform
	- Palo Alto
	- pfSense?
	- Others?
- get PC working
- get server working

Testing
- listen on the wire
- logs
	- server/pc
	- firewall
- monitoring (e.g. nagios)
- direct examination (scan, etc.)

Develop test cases / stories?
- external to internal
- internal to external

- is firewall up
- block everything
- allow www

- block known bad hosts
- block scans (openvas? )
