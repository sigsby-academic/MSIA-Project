#!/usr/bin/env bats

@test "external has correct default route" {
	vagrant ssh -c "ip route | grep -F 'default via 192.168.202.2'" external
}

@test "external can ping external switch" {
	vagrant ssh -c "ping -c1 192.168.202.1" external
}

@test "external can ping external firewall" {
	vagrant ssh -c "ping -c1 192.168.202.2" external
}

@test "external can ping external host (self)" {
	vagrant ssh -c "ping -c1 192.168.202.3" external
}

@test "external can't ping internal switch" {
	run vagrant ssh -c "ping -c1 192.168.201.1" external
	[ $status != 0 ]
}

@test "external can't ping internal firewall" {
	run vagrant ssh -c "ping -c1 192.168.201.2" external
	[ $status != 0 ]
}

@test "external can't ping internal host" {
	run vagrant ssh -c "ping -c1 192.168.201.3" external
	[ $status != 0 ]
}
