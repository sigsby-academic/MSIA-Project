#!/usr/bin/env bats

@test "internal has correct default route" {
	vagrant ssh -c "ip route | grep -F 'default via 192.168.201.2'" internal
}

@test "internal can ping internal switch" {
	vagrant ssh -c "ping -c1 192.168.201.1" internal
}

@test "internal can ping internal firewall" {
	vagrant ssh -c "ping -c1 192.168.201.2" internal
}

@test "internal can ping internal host (self)" {
	vagrant ssh -c "ping -c1 192.168.201.3" internal
}

@test "internal can't ping external switch" {
	run vagrant ssh -c "ping -c1 192.168.202.1" internal
	[ $status != 0 ]
}

@test "internal can't ping external firewall" {
	run vagrant ssh -c "ping -c1 192.168.202.2" internal
	[ $status != 0 ]
}

# demo rule test
@test "internal can ping external host" {
	vagrant ssh -c "ping -c1 192.168.202.3" internal
}
