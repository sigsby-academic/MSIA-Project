
cfg = {
	'firewall' => {
		'https'	=> '8443',
	},
}

[	'vagrant-vbguest', # https://github.com/dotless-de/vagrant-vbguest
	'vagrant-triggers', # https://github.com/emyl/vagrant-triggers/wiki/Trigger-recipes
	'vagrant-vbox-snapshot',
	'vagrant-hostmanager',
].each do |plugin|
	if ! Vagrant.has_plugin?("#{plugin}")
		system("vagrant plugin install #{plugin}")
	end
end

Vagrant.configure('2') do |c|

	# vagrant-vbguest plugin
	# disable fetching/installing virtualbox guest additons by default
	c.vbguest.auto_update = false
	c.vbguest.no_remote = true

	c.hostmanager.enabled = false
	c.hostmanager.include_offline = true
	c.hostmanager.manage_host = false

	# global virtualbox settings
	c.vm.provider 'virtualbox' do |t|
		#t.gui = true
		t.customize [
			'modifyvm', :id, '--groups','/msia'
		]
	end

	# vagrant shares the project folder with each vm, disable this by default
	c.vm.synced_folder '.', '/vagrant', disabled:true
	# c.vm.synced_folder '<host-path>', '<vm-mount-point>'

	# use vagrant-triggers and vagrant-vbox-snapshot plugins
	# to create a base snapshot of each vm before further provisioning
	c.vm.provision 'snapshot-basebox', type:'trigger', run:'always' do |t|
		t.fire do
			if ! `vagrant snapshot list #{@machine.name}`.match('base')
				run "vagrant snapshot take #{@machine.name} base"
			end
		end
	end

	# ansible provisioner
	# https://www.vagrantup.com/docs/provisioning/ansible_common.html
	c.vm.provision 'provision-ansible', type:'ansible' do |t|
		t.become = true
		t.playbook = 'ansible/vagrant.yml'
		t.tags = (ENV['TAGS'] ? ENV['TAGS'] : 'all')
		t.extra_vars = {}
		t.host_vars = {
			'firewall' => {
				'https' => cfg['firewall']['https']
			}
		}
	end

	# Palo Alto firewall host
	# see Makefile for how this was created
	# note that this vagrant box is unique
	# it has no virtualbox guest additions (impossible for that machine?)
	# the first interface is NAT and should have internet access
	# this first interface should also be the management interface
	c.vm.define 'firewall' do |h|
		h.vm.box = 'msia/PA-VM-7.1.0'

		# the Makefile should have added this box already
		#h.vm.box_url = 'tmp/PA-VM-7.1.0.box'

		# not sure if we can set the hostname in the usual way on this box
		#h.vm.hostname = 'firewall'

		# takes a while to boot :-)
		h.vm.boot_timeout = 900

		# forward management interface ports so we can access console & web UI
		#h.vm.network 'forwarded_port', guest:22, host:2222, id:'MGMT-SSH'
		h.vm.network 'forwarded_port', guest:443, host:cfg['firewall']['https']

		# internal network interfaces
		h.vm.network 'private_network', ip:'192.168.201.2', auto_config:false
		# example routing when using dhcp w/ route added (192.168.56.0/24):
		# Destination			Next-Hop				Interface
		# 0.0.0.0/0				192.168.201.1		ethernet1/1
		# 192.168.201.0/24	192.168.201.2		ethernet1/1
		# 192.168.201.2/32	0.0.0.0

		# external network interface
		h.vm.network 'private_network', ip:'192.168.202.2', auto_config:false

		# external interface (bridge)
		# doesn't work on all host NIC types (e.g., fails on USB phone tether?)
		# h.vm.network 'public_network', auto_config:false

		h.vm.provider 'virtualbox' do |t|
			#t.gui = true
			t.name = 'msia.firewall'
		end
		h.ssh.username = 'admin'
		h.ssh.private_key_path = 'tmp/id_rsa'
	end

	c.vm.define 'external' do |h|
		h.vm.box = 'centos/7'
		h.vm.hostname = 'external'
		h.vm.network 'private_network', ip:'192.168.202.3'
		h.vm.provider 'virtualbox' do |t|
			#t.gui = true
			t.name = 'msia.external'
		end
		c.vm.provision :hostmanager
	end

	c.vm.define 'internal' do |h|
		h.vm.box = 'centos/7'
		h.vm.hostname = 'internal'
		h.vm.network 'private_network', ip:'192.168.201.3'
		h.vm.provider 'virtualbox' do |t|
			#t.gui = true
			t.name = 'msia.internal'
			t.customize [
				'modifyvm', :id, '--groups','/msia'
			]
		end
		c.vm.provision :hostmanager
	end

end
