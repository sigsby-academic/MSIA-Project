
Exploring a Test-Driven Approach to Network Security Policy  
R. David Sigsby  
Davenport University, Fall Semester 2017  
Thesis Project for Master of Science in Information Assurance  

# Introduction

This is the proof of concept project for my Master's thesis.  This project
explores basic techniques for implementing network security, specifically
network firewall configuration and policy, using test-driven development
techniques.  This is enabled by advances in virtualization, software-defined
infrastructure, infrastructure as code, and test-driven infrastructure.

See the [thesis report](https://gitlab.com/sigsby-academic/MSIA-Report/-/blob/master/Final.Report.pdf) for more information.

# Requirements

This project was developeed using Arch Linux.

Software: virtualbox, vagrant, GNU make, ansible, bats
